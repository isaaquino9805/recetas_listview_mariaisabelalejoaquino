package com.example.recetas_listview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView

class listas : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listas)

        var recetas : ArrayList<String> = ArrayList()
        recetas.add("Crema de calabaza")
        recetas.add("Rollitos de pollo con queso")
        recetas.add("Pasta con camarones al ajo")
        recetas.add("Lasaña de patata")
        recetas.add("Coliflor al horno")
        recetas.add("Tostada de queso brie")
        recetas.add("Tartaletas de manzana")
        recetas.add("Salmón al horno")
        recetas.add("Mousse de café")
        recetas.add("Hojaldres de queso y setas")

        val lista = findViewById<ListView>(R.id.lista)
        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,recetas)
        lista.adapter=adaptador
        lista.onItemClickListener= AdapterView.OnItemClickListener{ adapterView, view, i, l ->

            when(recetas.get(i)){
                "Crema de calabaza"->{
                    val intent = Intent(this,activity_cremaCalabaza::class.java)
                    startActivity(intent)
                }
                "Rollitos de pollo con queso"->{
                    val intent = Intent(this,activity_rollitos::class.java)
                    startActivity(intent)
                }
                "Pasta con camarones al ajo"->{
                    val intent = Intent(this,activity_pasta_camarones::class.java)
                    startActivity(intent)
                }
                "Lasaña de patata"->{
                    val intent = Intent(this,activity_lasana_patata::class.java)
                    startActivity(intent)
                }
                "Coliflor al horno"->{
                    val intent = Intent(this,activity_coliflor_h::class.java)
                    startActivity(intent)
                }
                "Tostada de queso brie"->{
                    val intent = Intent(this,activity_tostada::class.java)
                    startActivity(intent)
                }
                "Tartaletas de manzana"->{
                    val intent = Intent(this,activity_tartaletas::class.java)
                    startActivity(intent)
                }
                "Salmón al horno"->{
                    val intent = Intent(this,activity_salmon::class.java)
                    startActivity(intent)
                }
                "Mousse de café"->{
                    val intent = Intent(this,activity_mousse::class.java)
                    startActivity(intent)
                }
                "Hojaldres de queso y setas"->{
                    val intent = Intent(this,activity_hojaldres::class.java)
                    startActivity(intent)
                }

            }
    }
    }
}