package com.example.recetas_listview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton = findViewById<Button> (R.id.btnIr)
        boton.setOnClickListener { val intent = Intent (this, listas::class.java)
            startActivity(intent)
        }
    }
}